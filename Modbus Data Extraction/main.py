from modbus import Modbus
import datetime
import pandas as pd 
import numpy as np 
from modbus import run


if __name__ == '__main__':
    m = Modbus()

    run(m, interval_minutes=1)