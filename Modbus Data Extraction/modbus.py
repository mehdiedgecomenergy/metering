import socket
import pandas as pd
import datetime
import numpy as np 
from apscheduler.schedulers.blocking import BlockingScheduler

class Modbus(object):
    def __init__(self, port=502, socket_ip='127.0.0.1',  buff_size=1024, unicode_data='utf-8', data_seperator='.'):
    
        """ You need just create instance from this class with these paramters

        :type port: int
        :param port: speicif port that script would run on it
        :default: 502
    
        :type socket_ip: string
        :param socket_ip: speicif ip that script would run on it for local host it is '127.0.0.1'
        :default: '127.0.0.1'
    
        :type buff_size: int
        :param buff_size: It determines how much data is in every block 
        :default: 1024
    
        :type unicode_data: string
        :param unicode_data:
        :default: 'utf-8'
    
        :type data_seperator: string
        :param data_seperator: Data seperator for spliting data 
        :default: '|'

        """
        self.port = port
        self.socket_ip = socket_ip
        self.buff_size = buff_size
        self.unicode_data = unicode_data
        self.data_seperator = data_seperator

    def read_data(self):
        print(f'Read data from this address {self.socket_ip}:{self.port}\n\n\n')
        output = ''
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            try: 
                s.connect((self.socket_ip, self.port))
                while True:
                    data = s.recv(self.buff_size)
                    output = output + self.__decode_data(data)
                    print(output)
                    if not data:
                        break
            except Exception as e:
                print(e) 
                return None
            return output


    def __decode_data(self, data):
        try: 
            return data.decode(self.unicode_data)
        except AttributeError as e: 
            print(e)
            return None


    def save_data(self, data):
        print('\n\n\n Save data as DataFrame \n\n\n')
        if data is None:
            return None
        pd.DataFrame(np.array(data.split(self.data_seperator))).to_csv(f'a.csv')
            
    def start_service(self):
        self.save_data(self.read_data())

def run(m, interval_minutes=10):

    """ This run whole project every interval time

    :type m: Modbus
    :param m: Create instance with specific paramaters

    :type interval_minutes: int
    :param interval_minutes: interval time between runs

    """
    bs = BlockingScheduler()
    bs.add_job(m.start_service, 'interval', minutes=interval_minutes)
    bs.start()



