# Modbus

#### By: Mohammadreza Parvizi
#### May 26, 2019
---
This project is the socket based application for getting data from specific port(modbus=502) at the localhost.

## 1- Install essential softwares and libraries

* Install anaconda on ubuntu
* conda install pandas
* conda install numpy 
* conda install datetime
* conda install -c conda-forge apscheduler

## 2- Code Review
For running this project just need to understand **main.py** 

```bash
#main.py

m = Modbus(port=502, socket_ip='127.0.0.1',  buff_size=1024, unicode_data='utf-8', data_seperator='|') #create object from mod

 """ 
 You need just create instance from this class with these paramters

:type port: int
:param port: speicif port that script would run on it
:default: 502

:type socket_ip: string
:param socket_ip: speicif ip that script would run on it for local host it is '127.0.0.1'
:default: '127.0.0.1'

:type buff_size: int
:param buff_size: It determines how much data is in every block 
:default: 1024

:type unicode_data: string
:param unicode_data:
:default: 'utf-8'

:type data_seperator: string
:param data_seperator: Data seperator for spliting data 
:default: '|'

"""
  
run(m, interval_minutes=10) #run it

""" 
This run whole project every interval time

:type m: Modbus
:param m: Create instance with specific paramaters

:type interval_minutes: int
:param interval_minutes: interval time between runs

"""
```

## 3- Service to run this project automatically
Run this project periodically at starting time need to create service like below:

```bash
Description=YOUR_DESCRIPTION
Wants=network.target
After=syslog.target network-online.target

[Service]
Type=simple
WorkingDirectory= /Users/user/Documents/modbus 
ExecStart=python main.py
Restart=on-failure
RestartSec=10
KillMode=process

[Install]
WantedBy=multi-user.target

```
*change WorkingDirectory to project's directory path on os*


For run this service: 
```bash
sudo systemctl start myservice.service
```

