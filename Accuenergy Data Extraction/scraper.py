import os
import numpy
import tempfile
import pandas as pd
import datetime
from apscheduler.schedulers.blocking import BlockingScheduler


# url of the meter page
url = 'http://10.4.30.21/basic.cgi'


def read_data():
    """ Read data from the meter page
    :return: numpy array
    """
    temp_file = tempfile.NamedTemporaryFile()

    try:
        os.system('wget -O {} {}'.format(temp_file.name, url))
        data = temp_file.read().decode()

        values = []
        for line in data.splitlines():
            try:
                value = float(line)
                values.append(value)
            except ValueError:
                print('Unable to convert "{}" to float'.format(line))

        return numpy.array(values)
    except Exception as e:
        print('Error in scraping data: ', e)

    return None


def save_data(data):
    """ save given data in database
    :param data: numpy array
    :return: None
    """

    if data is None:
        return None
        # Nothing to do yet, just print data
    pd.DataFrame(data).to_csv(f'{datetime.datetime.now()}.csv')
    


def scrap():
    """ read and save data
    :return: None
    """
    data = read_data()
    save_data(data)


def loop(interval=15):
    """ Run scraper in a loop with the given interval
    :param interval: interval of the loop in seconds
    :return: None
    """
    scheduler = BlockingScheduler()
    scheduler.add_job(scrap, 'interval', seconds=interval)
    scheduler.start()
